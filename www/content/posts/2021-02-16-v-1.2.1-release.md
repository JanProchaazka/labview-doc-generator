---
title: "Version 1.2.1, maintainance release with interface support"
date: 2021-02-16T10:02:10+02:00
tags: ["release","UML"]
draft: false
---

This new version is mainly a maintainance release, but it also brings you support for LabVIEW 2020 interfaces.

![LabVIEW Interfaces in UML Class diagram](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/antidoc-Uml-Interface.png)

Important note: [Antidoc CLI](https://www.vipm.io/package/wovalab_lib_antidoc_cli/) needs to be updated to be compatible with this version

## Antidoc v1.2.1

The package is directly available through [VI Package Manager](https://www.vipm.io/package/wovalab_lib_antidoc/).

### Release note

#### New features
* Handle LabVIEW interfaces --> [#117](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/117)

#### Changes
* Better integration of progress bar API --> [#99](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/99) and [#100](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/100)
* Icons replace event types string in DQMH event table --> [#104](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/104)
* Change UML Class diagram orientation --> [#124](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/124)

#### Bug fix
* Add reference to classy Diagram Viewer in Legal Information section --> [#97](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/97)
* Add config object input to all public VIs using the config --> [#98](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/98)
* Lib filtering not correctly apply on callers --> [#101](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/101)
* Wrong menu item name --> [#102](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/102)
* Error during UML generation due to interfaces --> [#116](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/116)
* Progress bar refresh for multi-target project --> [#118](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/118)
* Empty section displayed --> [#119](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/119)
* Missing item in library --> [#123](https://gitlab.com/wovalab/open-source/labview-doc-generator/-/issues/123)